class User < ActiveRecord::Base
	validates :email, presence: true, uniqueness: true
	validates :first_name, presence: true
	validates :last_name, presence: true
	has_and_belongs_to_many :projects

end
