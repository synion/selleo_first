class UsersController < ApplicationController
	helper_method :sort_column, :sort_direction
 	def index
		  @users = User.order(params[:sort])
		  @users = @users.to_a
		  @users.select! { |user| user.email.include?(params[:search].to_s) || user.first_name.include?(params[:search].to_s) ||  user.last_name.include?(params[:search].to_s) || user.projects.any? do |project| 
		  						project.name.include?(params[:search].to_s) 
		  					end}
		 

	end

	def show
		@user = User.find(params[:id])
	end

	def new
		@user = User.new
	end

	def create
    	@user = User.new(user_params)
    	if @user.save
    		redirect_to @user
  		else
    		render 'new'
  		end
 	end

 	private
 	def user_params
 		params.require(:user).permit(:first_name, :last_name, :email)
 	end	

  	def sort_column
    	User.column_names.include?(params[:sort]) ? params[:sort] : "first_name"
  	end
  
  	def sort_direction
    	%w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  	end
end
