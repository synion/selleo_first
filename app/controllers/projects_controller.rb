class ProjectsController < ApplicationController
  

	def index
		@projects = Project.all 
	end

	def addusers
	    @project = Project.find(params[:id])
	    @project.users << User.find(params[:user_id])
	    respond_to do |format|
	       format.html { redirect_to @project, :notice => 'Added.' }
	    end
  	end

  	def removeuser
     	  @project = Project.find(params[:id])
      	@user = User.find(params[:user_id])

      	if @user
         	@project.users.delete(@user)
      	end

      	respond_to do |format|
        	format.html { redirect_to @project, notice: 'User was successfully removed from Project' }
      	end
  	end

	def show
		@project = Project.find(params[:id])
		@users = User.all - @project.users
    @usersadd = @project.users.all
	end

	def new
		@project = Project.new
	end

	def create
    	@project = Project.new(project_params)
    	if @project.save
    		redirect_to @project
  		else
    		render 'new'
  		end
 	end

 	def project_params
 		params.require(:project).permit(:name, :active)
 	end	
end
